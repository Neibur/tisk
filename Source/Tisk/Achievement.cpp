#include "Achievement.h"

#include "Public/Engine.h"
#include "Engine/World.h"
#include "TiskGameInstance.h"
#include "AchievementManager.h"
#include "Blueprint/UserWidget.h"

// This is done with just a 'switch' to keep it simple.  Alternative solutions:
//  - Hierarchy of achievements based on inheritance with custom behaviour
//  - Data driven achievements defined in some textual format (eg. xml, json, etc)
Achievement::Achievement(EAchievement AchievementType, TWeakObjectPtr<UWorld> World)
{
	this->AchievementType = AchievementType;
	this->World = World;

	switch (AchievementType) {
	case EAchievement::ACHIEVEMENT_KILLED_ENEMY_1:
		this->Description = TEXT("Killed 1 Enemy");
		break;
	case EAchievement::ACHIEVEMENT_KILLED_ENEMY_5:
		this->Description = TEXT("Killed 5 Enemies");
		break;
	case EAchievement::ACHIEVEMENT_WON_LEVEL_1:
		this->Description = TEXT("Win 1 Level");
		break;
	case EAchievement::ACHIEVEMENT_WON_LEVEL_5:
		this->Description = TEXT("Win 5 Levels");
		break;
	}
}

EAchievement Achievement::GetType() const
{
	return AchievementType;
}

const FName& Achievement::GetDescription() const
{
	return Description;
}

void Achievement::RunBehaviour()
{
	// Call the Blueprint function "UnlockAchievement" from the AchievementWidget 
	// to play the "AchievementUnlocked" animation
	if (World.IsValid()) {
		TWeakObjectPtr<UUserWidget> AchievementWidget = World->GetGameInstance<UTiskGameInstance>()->AchievementManager->AchievementWidget_Instance;
		// Get the function as named in the Blueprint
		TWeakObjectPtr<UFunction> UnlockAchievementFunction = AchievementWidget->FindFunction(TEXT("UnlockAchievement"));
		if (UnlockAchievementFunction.IsValid()) {
			// Once we got the function, invoke it. Input parameters have to be provided
			// in a struct, in the same order they are defined in the Blueprint function.
			// In case the function has a return value, it will be specified as the last
			// member in the struct.
			AchievementWidget->ProcessEvent(UnlockAchievementFunction.Get(), { &Description });
		}
	}
}