#pragma once

#include "CoreMinimal.h"

// Forward declare it for the Achievement class to know it.
// This is done this way, so new achievement types can be added
// at the end of this file (in the enum).
enum class EAchievement : uint8;

class Achievement
{
public:
	// In this implementation, the class does need a reference to the World
	Achievement(EAchievement AchievementType, TWeakObjectPtr<class UWorld> World);

	EAchievement GetType() const;
	const FName& GetDescription() const;

	// This method could be overriden in child classes
	// to implement the custom behaviour of the concrete
	// achievement
	virtual void RunBehaviour();

private:
	TWeakObjectPtr<class UWorld> World;

	FName Description;
	EAchievement AchievementType;
};

UENUM(BlueprintType)
enum class EAchievement : uint8
{
	ACHIEVEMENT_KILLED_ENEMY_1	UMETA(DisplayName = "Killed 1 Enemy"),
	ACHIEVEMENT_KILLED_ENEMY_5	UMETA(DisplayName = "Killed 5 Enemies"),
	ACHIEVEMENT_WON_LEVEL_1		UMETA(DisplayName = "Won 1 Level"),
	ACHIEVEMENT_WON_LEVEL_5		UMETA(DisplayName = "Won 5 Levels"),

	// Used to retrieve the number of elements in the enum
	LAST_ACHIEVEMENT UMETA(Hidden)
};

