// Fill out your copyright notice in the Description page of Project Settings.


#include "AchievementManager.h"
#include "Kismet/GameplayStatics.h"
#include "Blueprint/UserWidget.h"
#include "ConstructorHelpers.h"
#include "CustomSaveGame.h"


bool UAchievementManager::Instantiated_ = false;

UAchievementManager::UAchievementManager()
{
	// Check if the class is being created as a CDO (i.e., at the editor startup).
	// If that's the case, we allow its creation without affecting the only-one-instance behaviour.
	if (!HasAnyFlags(RF_ClassDefaultObject)) {
		verifyf(!Instantiated_, TEXT("Trying to instance more than one class of UAchievementManager"));
		Instantiated_ = true;
	}

	TWeakObjectPtr<UCustomSaveGame> SaveGameInstance = LoadFull();
	if (SaveGameInstance.IsValid()) {
		EventsCounter.Add(EEvent::EVENT_KILL_ENEMY, SaveGameInstance->FireballEnemiesKilled);
		EventsCounter.Add(EEvent::EVENT_WIN_LEVEL, SaveGameInstance->LevelsWon);
	}
	else {
		EventsCounter.Add(EEvent::EVENT_KILL_ENEMY, 0);
		EventsCounter.Add(EEvent::EVENT_WIN_LEVEL, 0);

		SaveFull(NewObject<UCustomSaveGame>());
	}

	auto AchievementWidgetAsset = ConstructorHelpers::FClassFinder<UUserWidget>(TEXT("/Game/UI/Achievements/AchievementItemWidget"));
	if (AchievementWidgetAsset.Succeeded()) {
		AchievementWidgetClass = AchievementWidgetAsset.Class;
	}
}

UAchievementManager::~UAchievementManager()
{
	Instantiated_ = false;
}

UWorld* UAchievementManager::GetWorld() const
{
	return World.Get();
}

void UAchievementManager::SetWorld(TWeakObjectPtr<UWorld> World)
{
	this->World = World;


	if (!AchievementWidget_Instance) {
		if (AchievementWidgetClass) {
			// Create the widget and store it.
			AchievementWidget_Instance = CreateWidget<UUserWidget>(World->GetGameInstance(), AchievementWidgetClass);
		}
	}
}

void UAchievementManager::OnNotify(UObject* Entity, EEvent Event)
{
	TWeakObjectPtr<UCustomSaveGame> SaveGameInstance = LoadFull();

	switch (Event) {
	case EEvent::EVENT_KILL_ENEMY:
		IncreaseEventCounter(Event);
		SaveGameInstance->FireballEnemiesKilled++;
		if (EventsCounter[Event] == 1) {
			Unlock(EAchievement::ACHIEVEMENT_KILLED_ENEMY_1);
			SaveGameInstance->AchievementsUnlocked.Add(EAchievement::ACHIEVEMENT_KILLED_ENEMY_1, true);
		}
		else if (EventsCounter[Event] == 5) {
			Unlock(EAchievement::ACHIEVEMENT_KILLED_ENEMY_5);
			SaveGameInstance->AchievementsUnlocked.Add(EAchievement::ACHIEVEMENT_KILLED_ENEMY_5, true);
		}
		SaveFull(SaveGameInstance.Get());
		break;
	case EEvent::EVENT_WIN_LEVEL:
		IncreaseEventCounter(Event);
		SaveGameInstance->LevelsWon++;

		if (EventsCounter[Event] == 1) {
			Unlock(EAchievement::ACHIEVEMENT_WON_LEVEL_1);
			SaveGameInstance->AchievementsUnlocked.Add(EAchievement::ACHIEVEMENT_WON_LEVEL_1, true);
		}
		else if (EventsCounter[Event] == 5) {
			Unlock(EAchievement::ACHIEVEMENT_WON_LEVEL_5);
			SaveGameInstance->AchievementsUnlocked.Add(EAchievement::ACHIEVEMENT_WON_LEVEL_5, true);
		}
		SaveFull(SaveGameInstance.Get());
		break;
	}
}

/* Store the CustomSaveGame object given */
void UAchievementManager::SaveFull(UCustomSaveGame* SaveGame)
{
	UGameplayStatics::SaveGameToSlot(SaveGame, SaveGame->SaveSlotName, SaveGame->UserIndex);
}

/* Retrieve the CustomSaveGame object */
UCustomSaveGame* UAchievementManager::LoadFull()
{
	TWeakObjectPtr<UCustomSaveGame> LoadGameInstance = Cast<UCustomSaveGame>(UGameplayStatics::CreateSaveGameObject(UCustomSaveGame::StaticClass()));
	LoadGameInstance = Cast<UCustomSaveGame>(UGameplayStatics::LoadGameFromSlot(LoadGameInstance->SaveSlotName, LoadGameInstance->UserIndex));
	return LoadGameInstance.Get();
}

/* Resets the values of the stored CustomSaveGame object */
void UAchievementManager::ResetSave()
{
	TWeakObjectPtr<UCustomSaveGame> NewSaveGame = NewObject<UCustomSaveGame>();
	UGameplayStatics::SaveGameToSlot(NewSaveGame.Get(), NewSaveGame->SaveSlotName, NewSaveGame->UserIndex);
}

void UAchievementManager::IncreaseEventCounter(EEvent Event)
{
	// We need to keep a counter of the number of events that have arriven,
	// in order to unlock the appropriate achievements.
	// But we do not need to increase EVERY event; just the ones related to
	// the achievements.
	EventsCounter.Add(Event, ++EventsCounter[Event]);
}

void UAchievementManager::Unlock(EAchievement AchievementType)
{
	// Save the achievement by its type, just in case we want to do something
	// with it in the future
	Achievement CurrentAchievement = Achievement(AchievementType, World);
	Achievements.Add(AchievementType, CurrentAchievement);

	// Run the achievement custom behaviour
	CurrentAchievement.RunBehaviour();
}