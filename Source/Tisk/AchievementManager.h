// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
//#include "UObject/NoExportTypes.h"
#include "Achievement.h"
#include "AchievementObserver.h"
#include "AchievementManager.generated.h"

/**
 * 
 */
UCLASS()
class TISK_API UAchievementManager : public UObject, public IAchievementObserver
{
	GENERATED_BODY()
	

public:
	UAchievementManager();
	~UAchievementManager();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<class UUserWidget> AchievementWidgetClass;

	// Variable to hold the widget After Creating it.
	UPROPERTY()
		class UUserWidget* AchievementWidget_Instance;

	class UWorld* GetWorld() const override;
	void SetWorld(TWeakObjectPtr<UWorld> World);

	void OnNotify(UObject* Entity, EEvent Event) override;

	UFUNCTION(BlueprintCallable)
		void SaveFull(class UCustomSaveGame* SaveGame);

	UFUNCTION(BlueprintCallable)
		class UCustomSaveGame* LoadFull();

	UFUNCTION(BlueprintCallable)
		void ResetSave();

private:
	void IncreaseEventCounter(EEvent Event);
	void Unlock(EAchievement AchievementType);

private:
	TWeakObjectPtr<UWorld> World;

	TMap<EEvent, uint32> EventsCounter;
	TMap<EAchievement, Achievement> Achievements;

	static bool Instantiated_;
};
