// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Event.h"
#include "AchievementObserver.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UAchievementObserver : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TISK_API IAchievementObserver
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual void OnNotify(UObject* Entity, EEvent Event) = 0;
};
