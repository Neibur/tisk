// Fill out your copyright notice in the Description page of Project Settings.


#include "AchievementSubject.h"
#include "AchievementObserver.h"


void UAchievementSubject::AddObserver(TScriptInterface<IAchievementObserver> Observer)
{
	Observers.Add((IAchievementObserver*)Observer.GetInterface());
}

void UAchievementSubject::RemoveObserver(TScriptInterface<IAchievementObserver> Observer)
{
	Observers.Remove((IAchievementObserver*)Observer.GetInterface());
}

void UAchievementSubject::Notify(UObject* Entity, EEvent Event)
{
	for (auto Observer : Observers) {
		if (Observer != nullptr) {
			Observer->OnNotify(Entity, Event);
		}
	}
}
