// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
//#include "UObject/NoExportTypes.h"
#include "Event.h"
#include "AchievementSubject.generated.h"

/**
 * 
 */
UCLASS()
class TISK_API UAchievementSubject : public UObject
{
	GENERATED_BODY()
	

public:
	UFUNCTION(BlueprintCallable, Category = "Observer Pattern")
		void AddObserver(TScriptInterface<IAchievementObserver> Observer);

	UFUNCTION(BlueprintCallable, Category = "Observer Pattern")
		void RemoveObserver(TScriptInterface<IAchievementObserver> Observer);

	UFUNCTION(BlueprintCallable, Category = "Observer Pattern")
		void Notify(UObject* Entity, EEvent Event);

private:
	// UINTERFACES generate their destructor private,
	// so shared pointer can't access it to manage its life cycle,
	// thus giving us an error at compile time if we try to use them.
	TArray<IAchievementObserver*> Observers;
};
