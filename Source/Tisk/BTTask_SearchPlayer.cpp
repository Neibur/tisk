// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_SearchPlayer.h"
#include "Engine/World.h"
#include "EngineMinimal.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTreeComponent.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BlackboardComponent.h"


EBTNodeResult::Type UBTTask_SearchPlayer::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	TWeakObjectPtr<UBlackboardComponent> blackboard = OwnerComp.GetBlackboardComponent();

	TWeakObjectPtr<AActor> SelfActor = Cast<AActor>(blackboard->GetValueAsObject(FName("SelfActor")));
	TWeakObjectPtr<AActor> PlayerActor = GetWorld()->GetFirstPlayerController()->GetPawn();

	if (PlayerActor.IsValid()) {
		blackboard->SetValueAsObject(FName("PlayerActor"), PlayerActor.Get());
		//UE_LOG(LogTemp, Warning, TEXT("%f"), SelfActor->GetDistanceTo(PlayerActor.Get()));
		float distanceToPlayer =  SelfActor->GetDistanceTo(PlayerActor.Get());
		// Player is close enough to chase
		if (distanceToPlayer <= 600.0f) {
			blackboard->SetValueAsVector(FName("PlayerLocation"), PlayerActor.Get()->GetActorLocation());
			blackboard->SetValueAsBool(FName("bSeeking"), true);
			blackboard->SetValueAsBool(FName("bOutOfRange"), false);
		}
		else {
			blackboard->SetValueAsBool(FName("bOutOfRange"), true);
			// If we are Seeking the player he has to go further in order to stop chasing him
			if (blackboard->GetValueAsBool(FName("bSeeking"))) {
				if (distanceToPlayer <= 1500.0f) {
					blackboard->SetValueAsVector(FName("PlayerLocation"), PlayerActor.Get()->GetActorLocation());
				}
			}
			else {
				blackboard->SetValueAsVector(FName("PlayerLocation"), FVector(0, 0, 0));
			}
		}
	}

	//UE_LOG(LogTemp, Warning, TEXT("Searched"));

	return EBTNodeResult::Succeeded;
}