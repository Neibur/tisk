// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_ShootPlayer.h"
#include "Engine/World.h"
#include "EngineMinimal.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTreeComponent.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BlackboardComponent.h"
#include "FireballEnemy.h"
#include "BasicFireball.h"
#include "Sound/SoundCue.h"


EBTNodeResult::Type UBTTask_ShootPlayer::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	TWeakObjectPtr<UBlackboardComponent> blackboard = OwnerComp.GetBlackboardComponent();

	TWeakObjectPtr<AActor> SelfActor = Cast<AActor>(blackboard->GetValueAsObject(FName("SelfActor")));
	TWeakObjectPtr<AActor> PlayerActor = Cast<AActor>(blackboard->GetValueAsObject(FName("PlayerActor")));

	if (PlayerActor.IsValid()) {
		FVector VectorToPlayer = PlayerActor.Get()->GetActorLocation() - SelfActor.Get()->GetActorLocation();
		GetWorld()->SpawnActor<ABasicFireball>(
			(SelfActor.Get()->GetActorLocation() + VectorToPlayer.GetClampedToSize(0.0f, 75.0f))
			, VectorToPlayer.Rotation());

		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), Cast<AFireballEnemy>(SelfActor)->ShootSound.Get(), SelfActor->GetActorLocation());

		// Set the shooting cooldown
		blackboard->SetValueAsFloat(FName("ShootingCooldown"), FMath::FRandRange(1.0f, 3.0f));
	}

	return EBTNodeResult::Succeeded;
}