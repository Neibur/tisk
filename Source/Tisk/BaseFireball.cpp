// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseFireball.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Engine/EngineTypes.h"
#include "EngineMinimal.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "Sound/SoundCue.h"

// Sets default values
ABaseFireball::ABaseFireball()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	USceneComponent* DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;

	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	BoxCollision->SetupAttachment(RootComponent);
	BoxCollision->SetBoxExtent(FVector(32.0f, 32.0f, 32.0f));
	BoxCollision->SetWorldScale3D(FVector(1.0f, 1.0f, 1.5f));
	BoxCollision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	BoxCollision->SetCollisionProfileName(FName("OverlapAllDynamic"));

	auto fireballParticleSystemAsset = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/Enemies/Particles/P_ky_fireBall.P_ky_fireBall'"));
	if (fireballParticleSystemAsset.Succeeded())
	{
		fireballFx = fireballParticleSystemAsset.Object;
	}

	auto hitParticleSystemAsset = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/Enemies/Particles/P_ky_explosion.P_ky_explosion'"));
	if (hitParticleSystemAsset.Succeeded())
	{
		hitFx = hitParticleSystemAsset.Object;
	}

	auto ExplosionSoundCueAsset = ConstructorHelpers::FObjectFinder<USoundCue>(TEXT("SoundCue'/Game/Audio/Explosion_Cue.Explosion_Cue'"));
	if (ExplosionSoundCueAsset.Succeeded())
	{
		ExplosionSound = ExplosionSoundCueAsset.Object;
	}

	Tags.Add(FName("ky_shot"));

	speed = 1000.0f;
	speedCorrect = 0.1f;
	duration = 2.0f;
	disabled = false;
}

// Called when the game starts or when spawned
void ABaseFireball::BeginPlay()
{
	Super::BeginPlay();

	fireballEmitter = UGameplayStatics::SpawnEmitterAttached(fireballFx.Get(), RootComponent);
}

// Called every frame
void ABaseFireball::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABaseFireball::BasicForwardMovement(float DeltaTime)
{
	AddActorLocalOffset(FVector(speed * DeltaTime, 0.0f, 0.0f));
}