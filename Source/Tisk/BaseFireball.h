// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseFireball.generated.h"

UCLASS()
class TISK_API ABaseFireball : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseFireball();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/*UFUNCTION()
		void ActorOverlapBegin(AActor* Me, AActor* OtherActor);*/

	float speed;
	float duration;

protected:
	TWeakObjectPtr<class UBoxComponent> BoxCollision;

	float speedCorrect;

	TWeakObjectPtr<class UParticleSystem> fireballFx;
	TWeakObjectPtr<class UParticleSystem> hitFx;
	TWeakObjectPtr<class UParticleSystemComponent> fireballEmitter;

	TWeakObjectPtr<class USoundCue> ExplosionSound;

	bool disabled;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//virtual void RunBehaviour() PURE_VIRTUAL(ABaseFireball::RunBehaviour, );

	void BasicForwardMovement(float DeltaTime);

};
