// Fill out your copyright notice in the Description page of Project Settings.


#include "BasicFireball.h"
#include "EngineMinimal.h"
#include "TiskCharacter.h"
#include "SparkBallAbility.h"
#include "Sound/SoundCue.h"


ABasicFireball::ABasicFireball()
{
	OnActorBeginOverlap.AddDynamic(this, &ABasicFireball::ActorOverlapBegin);
}

void ABasicFireball::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	BasicForwardMovement(DeltaTime);
}

void ABasicFireball::ActorOverlapBegin(AActor* Me, AActor* OtherActor)
{
	//UE_LOG(LogTemp, Warning, TEXT("BasicFireball: ActorOverlapBegin"));

	if (OtherActor != nullptr) {
		if (OtherActor->ActorHasTag(FName("ParryShield"))
			|| OtherActor->ActorHasTag(FName("FireballEnemy"))
			|| OtherActor->ActorHasTag(FName("ChargedSparkBall"))
			|| OtherActor->IsA(ABaseFireball::StaticClass())) {
			
		}
		else {
			if (OtherActor->IsA(ATiskCharacter::StaticClass())) {
				Cast<ATiskCharacter>(OtherActor)->GetHit(1);
			}
			else if (OtherActor->IsA(ASparkBallAbility::StaticClass())) {
				Cast<ASparkBallAbility>(OtherActor)->GetHit(1);
			}
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), hitFx.Get(), GetActorTransform(), true, EPSCPoolMethod::None);
			UGameplayStatics::SpawnSoundAtLocation(GetWorld(), ExplosionSound.Get(), GetActorLocation());
			Destroy();
		}
	}
}