// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseFireball.h"
#include "BasicFireball.generated.h"

/**
 * 
 */
UCLASS()
class TISK_API ABasicFireball : public ABaseFireball
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABasicFireball();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void ActorOverlapBegin(AActor* Me, AActor* OtherActor);
};
