// Fill out your copyright notice in the Description page of Project Settings.


#include "CustomSaveGame.h"

UCustomSaveGame::UCustomSaveGame() {
	SaveSlotName = TEXT("FirstSaveSlot");
	UserIndex = 0;

	FireballEnemiesKilled = 0;
	LevelsWon = 0;

	// Fill the Achievements TMap
	AchievementsUnlocked.Add(EAchievement::ACHIEVEMENT_KILLED_ENEMY_1, false);
	AchievementsUnlocked.Add(EAchievement::ACHIEVEMENT_KILLED_ENEMY_5, false);
	AchievementsUnlocked.Add(EAchievement::ACHIEVEMENT_WON_LEVEL_1, false);
	AchievementsUnlocked.Add(EAchievement::ACHIEVEMENT_WON_LEVEL_5, false);
}