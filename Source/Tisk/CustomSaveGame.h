// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "Achievement.h"
#include "CustomSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class TISK_API UCustomSaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:
	UCustomSaveGame();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FString SaveSlotName;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int UserIndex;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Basic)
		int FireballEnemiesKilled;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Basic)
		int LevelsWon;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Basic)
		TMap<EAchievement, bool> AchievementsUnlocked;

	/* Used to Save/Load int values */
	int BufferValueInt;
};
