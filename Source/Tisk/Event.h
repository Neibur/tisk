#pragma once


UENUM(BlueprintType)
enum class EEvent : uint8 {
	EVENT_KILL_ENEMY		UMETA(DisplayName = "Event: Kill Enemy"),
	EVENT_WIN_LEVEL			UMETA(DisplayName = "Event: Win Level"),

	// Used to retrieve the number of elements in the enum
	LAST_EVENT          UMETA(Hidden)
};