// Fill out your copyright notice in the Description page of Project Settings.


#include "FireballEnemy.h"

#include "EngineMinimal.h"
#include "Components/CapsuleComponent.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "AIController.h"
#include "TiskGameModeBase.h"
#include "TiskGameInstance.h"
#include "AchievementSubject.h"
#include "ParriedFireball.h"
#include "Sound/SoundCue.h"

// Sets default values
AFireballEnemy::AFireballEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GetCapsuleComponent()->InitCapsuleSize(42.0f, 42.0f);

	USphereComponent* SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	SphereCollision->SetupAttachment(RootComponent);
	SphereCollision->SetSphereRadius(40.0f);
	SphereCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	auto FireParticleSystemAsset = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/Enemies/Particles/P_FireballEnemy.P_FireballEnemy'"));
	if (FireParticleSystemAsset.Succeeded())
	{
		FireParticleSystem = FireParticleSystemAsset.Object;
	}

	auto DeathParticleSystemAsset = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/Enemies/Particles/P_FireballEnemy_Death.P_FireballEnemy_Death'"));
	if (DeathParticleSystemAsset.Succeeded())
	{
		DeathParticleSystem = DeathParticleSystemAsset.Object;
	}

	auto AIControllerClassBP = ConstructorHelpers::FClassFinder<AAIController>(TEXT("/Game/Enemies/Blueprints/AIController_FireballEnemy"));
	if (AIControllerClassBP.Succeeded())
	{
		AIControllerClass = AIControllerClassBP.Class;
	}

	auto FireSoundCueAsset = ConstructorHelpers::FObjectFinder<USoundCue>(TEXT("SoundCue'/Game/Audio/FireballEnemy_Cue.FireballEnemy_Cue'"));
	if (FireSoundCueAsset.Succeeded())
	{
		FireSound = FireSoundCueAsset.Object;
	}

	auto ShootSoundCueAsset = ConstructorHelpers::FObjectFinder<USoundCue>(TEXT("SoundCue'/Game/Audio/Shoot2_Cue.Shoot2_Cue'"));
	if (ShootSoundCueAsset.Succeeded())
	{
		ShootSound = ShootSoundCueAsset.Object;
	}

	auto ExplosionSoundCueAsset = ConstructorHelpers::FObjectFinder<USoundCue>(TEXT("SoundCue'/Game/Audio/Explosion_Cue.Explosion_Cue'"));
	if (ExplosionSoundCueAsset.Succeeded())
	{
		ExplosionSound = ExplosionSoundCueAsset.Object;
	}

	Tags.Add(FName("FireballEnemy"));

	OnActorBeginOverlap.AddDynamic(this, &AFireballEnemy::ActorOverlapBegin);

	maxLife = 1;
	currentLife = maxLife;
	bDisappearing = false;
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
}

// Called when the game starts or when spawned
void AFireballEnemy::BeginPlay()
{
	Super::BeginPlay();
	
	Emitter = UGameplayStatics::SpawnEmitterAttached(FireParticleSystem.Get(), RootComponent);
	FireAudioComponent = UGameplayStatics::SpawnSoundAttached(FireSound.Get(), RootComponent);
}

// Called every frame
void AFireballEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AFireballEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AFireballEnemy::ActorOverlapBegin(AActor* Me, AActor* OtherActor)
{
	//UE_LOG(LogTemp, Warning, TEXT("ActorOverlapBegin"));
	if (OtherActor != nullptr) {
		if (OtherActor->IsA(AParriedFireball::StaticClass())) {
			GetHit(1);
			OtherActor->Destroy();
		}
	}
}

void AFireballEnemy::GetHit(float damage)
{
	currentLife -= damage;

	if (currentLife == 0) {
		Disappear();
	}
}

void AFireballEnemy::Disappear()
{
	if (!bDisappearing) {
		bDisappearing = true;
		// Disable the actor
		SetActorHiddenInGame(true);
		SetActorEnableCollision(false);
		SetActorTickEnabled(false);

		// Notify the GameMode
		GetWorld()->GetAuthGameMode<ATiskGameModeBase>()->KillEnemy();

		FireAudioComponent.Get()->Deactivate();

		// Death animation
		Emitter.Get()->DestroyComponent();
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), DeathParticleSystem.Get(), GetActorLocation());

		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), ExplosionSound.Get(), GetActorLocation());

		UAchievementSubject* aS = GetGameInstance<UTiskGameInstance>()->AchievementSubject;
		if (aS) {
			GetGameInstance<UTiskGameInstance>()->AchievementSubject->Notify(this, EEvent::EVENT_KILL_ENEMY);
		}

		SetLifeSpan(10.0f);
	}
}