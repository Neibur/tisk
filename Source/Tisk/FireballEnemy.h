// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FireballEnemy.generated.h"

UCLASS()
class TISK_API AFireballEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AFireballEnemy();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
		void ActorOverlapBegin(class AActor* Me, class AActor* OtherActor);

	void GetHit(float damage);

	TWeakObjectPtr<class USoundCue> ShootSound;

protected:
	float maxLife;
	float currentLife;

	bool bDisappearing;

	TWeakObjectPtr<class UParticleSystem> FireParticleSystem;
	TWeakObjectPtr<class UParticleSystem> DeathParticleSystem;
	TWeakObjectPtr<class UParticleSystemComponent> Emitter;

	TWeakObjectPtr<class USoundCue> FireSound;
	TWeakObjectPtr<class USoundCue> ExplosionSound;
	TWeakObjectPtr<class UAudioComponent> FireAudioComponent;

	FTimerHandle DeathHandle;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void Disappear();
};
