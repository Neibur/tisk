// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "LevelHUD.generated.h"

/**
 * 
 */
UCLASS()
class TISK_API ALevelHUD : public AHUD
{
	GENERATED_BODY()
	

public:
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void SetInputModeToGame();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void ResetWidgets();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void ShowLevelEndWidget(bool PlayerWon);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void ReceiveNavRequest(int NavigationType);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
		void TriggerButton();
};
