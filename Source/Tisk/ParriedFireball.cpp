// Fill out your copyright notice in the Description page of Project Settings.


#include "ParriedFireball.h"
#include "EngineMinimal.h"
#include "FireballEnemy.h"
#include "Sound/SoundCue.h"


AParriedFireball::AParriedFireball()
{
	OnActorBeginOverlap.AddDynamic(this, &AParriedFireball::ActorOverlapBegin);
}

void AParriedFireball::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	BasicForwardMovement(DeltaTime);
}

void AParriedFireball::ActorOverlapBegin(AActor* Me, AActor* OtherActor)
{
	//UE_LOG(LogTemp, Warning, TEXT("ParriedFireball: ActorOverlapBegin"));

	if (OtherActor != nullptr) {
		if (OtherActor->ActorHasTag(FName("TiskCharacter"))
			|| OtherActor->ActorHasTag(FName("ParryShield"))
			|| OtherActor->ActorHasTag(FName("SparkBall"))
			|| OtherActor->IsA(ABaseFireball::StaticClass())) {
			
		}
		else {
			if (OtherActor->IsA(AFireballEnemy::StaticClass())) {
				Cast<AFireballEnemy>(OtherActor)->GetHit(1);
			}
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), hitFx.Get(), GetActorTransform(), true, EPSCPoolMethod::None);
			UGameplayStatics::SpawnSoundAtLocation(GetWorld(), ExplosionSound.Get(), GetActorLocation());
			Destroy();
		}
	}
}
