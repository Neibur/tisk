// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseFireball.h"
#include "ParriedFireball.generated.h"

/**
 * 
 */
UCLASS()
class TISK_API AParriedFireball : public ABaseFireball
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AParriedFireball();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void ActorOverlapBegin(AActor* Me, AActor* OtherActor);
};
