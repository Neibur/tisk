// Fill out your copyright notice in the Description page of Project Settings.


#include "ParryShield.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Engine/EngineTypes.h"
#include "EngineMinimal.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "BasicFireball.h"
#include "ParriedFireball.h"

// Sets default values
AParryShield::AParryShield()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//DefaultCollisionChannel = ECollisionChannel::ECC_GameTraceChannel1;

	USceneComponent* DummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Dummy0"));
	RootComponent = DummyRoot;

	USphereComponent* SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	SphereCollision->SetupAttachment(RootComponent);
	SphereCollision->SetSphereRadius(90.0f);
	SphereCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	auto ShieldParticleSystemAsset = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/Player/Particles/P_ParryShieldShield.P_ParryShieldShield'"));
	if (ShieldParticleSystemAsset.Succeeded())
	{
		ShieldParticleSystem = ShieldParticleSystemAsset.Object;
	}

	OnActorBeginOverlap.AddDynamic(this, &AParryShield::ActorOverlapBegin);

	bAlreadyBlocked = false;
	maxTimeAlive = 0.3f;
	timeAlive = 0.0f;
}

// Called when the game starts or when spawned
void AParryShield::BeginPlay()
{
	Super::BeginPlay();
	
	Tags.Add(FName("ParryShield"));

	Emitter = UGameplayStatics::SpawnEmitterAttached(ShieldParticleSystem.Get(), RootComponent);
}

// Called every frame
void AParryShield::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	timeAlive += DeltaTime;
	if (timeAlive >= maxTimeAlive) {
		Emitter.Get()->DestroyComponent();
		Destroy();
	}
}


void AParryShield::ActorOverlapBegin(AActor* Me, AActor* OtherActor)
{
	if (OtherActor != nullptr) {
		if (OtherActor->IsA(ABasicFireball::StaticClass()) && !bAlreadyBlocked) {
			bAlreadyBlocked = true;
			FRotator FireballRotation = OtherActor->GetActorRotation();
			OtherActor->Destroy();

			FRotator ParriedFireballRotation = FRotator(0.0f, FireballRotation.Yaw - 180.0f, 0.0f);

			GetWorld()->SpawnActor<AParriedFireball>(
				(GetActorLocation() + ParriedFireballRotation.Vector().GetClampedToSize(0.0f, 75.0f))
				, ParriedFireballRotation);
		}
	}
}
