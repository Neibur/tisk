// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ParryShield.generated.h"

UCLASS()
class TISK_API AParryShield : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AParryShield();

	UFUNCTION()
		void ActorOverlapBegin(AActor* Me, AActor* OtherActor);

protected:
	bool bAlreadyBlocked;
	float timeAlive;
	float maxTimeAlive;

	TWeakObjectPtr<UParticleSystem> ShieldParticleSystem;

	TWeakObjectPtr<class UParticleSystemComponent> Emitter;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
