// Fill out your copyright notice in the Description page of Project Settings.


#include "SparkBallAbility.h"
#include "EngineMinimal.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "TiskCharacter.h"
#include "FireballEnemy.h"
#include "BasicFireball.h"
#include "NavAreas/NavArea_Obstacle.h"

// Sets default values
ASparkBallAbility::ASparkBallAbility()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	RootComponent = SphereCollision.Get();
	//SphereCollision->SetupAttachment(RootComponent);
	SphereCollision->SetSphereRadius(90.0f);
	SphereCollision->SetCollisionProfileName(FName("SparkBallAbility"));
	SphereCollision->SetSimulatePhysics(true);
	SphereCollision->SetLinearDamping(10.0f);
	SphereCollision->GetBodyInstance()->bLockZTranslation = true;

	// Attack Collision Sphere
	AttackSphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("AttackSphereCollision"));
	AttackSphereCollision->SetupAttachment(RootComponent);
	AttackSphereCollision->SetSphereRadius(1.0f);
	AttackSphereCollision->bDynamicObstacle = true;
	AttackSphereCollision->SetCanEverAffectNavigation(true);
	AttackSphereCollision->AreaClass = UNavArea_Obstacle::StaticClass();

	auto SparkParticleSystemAsset = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/Player/Particles/P_SparkBall.P_SparkBall'"));
	if (SparkParticleSystemAsset.Succeeded())
	{
		SparkParticleSystem = SparkParticleSystemAsset.Object;
	}

	auto ChargedSparkParticleSystemAsset = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/Player/Particles/P_SparkBall_Charged.P_SparkBall_Charged'"));
	if (ChargedSparkParticleSystemAsset.Succeeded())
	{
		ChargedSparkParticleSystem = ChargedSparkParticleSystemAsset.Object;
	}

	auto AttackRangeParticleSystemAsset = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/Player/Particles/P_SparkBall_AttackRange.P_SparkBall_AttackRange'"));
	if (AttackRangeParticleSystemAsset.Succeeded())
	{
		AttackRangeParticleSystem = AttackRangeParticleSystemAsset.Object;
	}

	auto AttackParticleSystemAsset = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/Player/Particles/P_SparkBall_Attack.P_SparkBall_Attack'"));
	if (AttackParticleSystemAsset.Succeeded())
	{
		AttackParticleSystem = AttackParticleSystemAsset.Object;
	}

	auto DeathParticleSystemAsset = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/Player/Particles/P_SparkBall_Death.P_SparkBall_Death'"));
	if (DeathParticleSystemAsset.Succeeded())
	{
		DeathParticleSystem = DeathParticleSystemAsset.Object;
	}

	SphereCollision->OnComponentBeginOverlap.AddDynamic(this, &ASparkBallAbility::InnerSphereOverlapBegin);
	AttackSphereCollision->OnComponentBeginOverlap.AddDynamic(this, &ASparkBallAbility::AttackSphereOverlapBegin);

	maxLife = 3;
	currentLife = maxLife;
	bCharged = false;
	bDisappearing = false;
	bAttackAvailable = false;
	attackCooldown = 2.0f;
	maxAttacks = 1;
	attacksLeft = maxAttacks;
	maxTimeAlive = 5.0f;
	timeAliveLeft = maxTimeAlive;
	movementSpeed = 0.0f;
	MovementDirection = FVector(0, 0, 0);
}

// Called when the game starts or when spawned
void ASparkBallAbility::BeginPlay()
{
	Super::BeginPlay();

	Tags.Add(FName("SparkBall"));
	SphereCollision->SetMassOverrideInKg(NAME_None, 100.0f, true);
	SphereCollision->UpdateBodySetup();

	Emitter = UGameplayStatics::SpawnEmitterAttached(SparkParticleSystem.Get(), RootComponent);
}

// Called every frame
void ASparkBallAbility::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bCharged) {
		timeAliveLeft -= DeltaTime;

		if (timeAliveLeft <= 0.0f) {
			Disappear();
		} else if (bAttackAvailable) {
			PerformAttack();
		}
	}
}

void ASparkBallAbility::AttackSphereOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//UE_LOG(LogTemp, Warning, TEXT("ASparkBallAbility ComponentOverlapBegin"));
	if (OtherActor != nullptr) {
		if (OtherActor->IsA(AFireballEnemy::StaticClass())
			&& bAttackAvailable) {
			PerformAttack();
		}
	}
}

void ASparkBallAbility::InnerSphereOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor != nullptr) {
		if (OtherActor->IsA(ABasicFireball::StaticClass())) {

		}
		else if (OtherActor->IsA(ATiskCharacter::StaticClass())) {
			FVector PlayerVel = OtherActor->GetVelocity();
			if ((FMath::Abs(PlayerVel.X) + FMath::Abs(PlayerVel.Y)) > 1000) {
				GetDashedByPlayer(OtherActor);
			}
		}
	}
}

void ASparkBallAbility::Charge()
{
	bCharged = true;
	timeAliveLeft = maxTimeAlive;
	Tags.Add(FName("ChargedSparkBall"));

	Emitter.Get()->DestroyComponent();
	Emitter = UGameplayStatics::SpawnEmitterAttached(ChargedSparkParticleSystem.Get(), RootComponent);
	AttackRangeEmitter = UGameplayStatics::SpawnEmitterAttached(AttackRangeParticleSystem.Get(), RootComponent);
	AttackEmitter = UGameplayStatics::SpawnEmitterAttached(AttackParticleSystem.Get(), RootComponent);
	AttackEmitter->bAllowRecycling = false;
	AttackEmitter->bAutoDestroy = false;

	AttackSphereCollision->SetSphereRadius(200.0f);

	FTimerHandle Handle;
	GetWorld()->GetTimerManager().SetTimer(Handle,
		[this] {
		bAttackAvailable = true;
	}
	, 0.5f, false, 0.5f);
}

void ASparkBallAbility::PerformAttack()
{
	if (bCharged && bAttackAvailable) {

		TArray<AActor*> OverlapingEnemies;
		AttackSphereCollision->GetOverlappingActors(OverlapingEnemies, AFireballEnemy::StaticClass());

		if (OverlapingEnemies.Num() > 0) {
			if (OverlapingEnemies[0] != nullptr && OverlapingEnemies[0]->IsA(AFireballEnemy::StaticClass())) {
				timeAliveLeft = maxTimeAlive;

				// Attack particle
				EnemyToAttack = Cast<AFireballEnemy>(OverlapingEnemies[0]);

				float distanceToEnemy = FVector::Distance(GetActorLocation(), EnemyToAttack->GetActorLocation());
				AttackEmitter->SetFloatParameter(FName("BeamDistance"), distanceToEnemy);
				AttackEmitter->SetWorldRotation((EnemyToAttack->GetActorLocation() - GetActorLocation()).Rotation());
				AttackEmitter.Get()->SetAbsolute(false, true);
				AttackEmitter.Get()->ActivateSystem(true);
				if (AttackEmitter.IsValid()) {
					UE_LOG(LogTemp, Warning, TEXT("Valid"));
				}
				else {
					UE_LOG(LogTemp, Warning, TEXT("NO Valid"));
				}

				// Deactivate emitter
				FTimerHandle Handle;
				GetWorld()->GetTimerManager().SetTimer(Handle,
					[this] {
					if (EnemyToAttack.IsValid()) {
						EnemyToAttack.Get()->GetHit(1);
					}
					if (AttackEmitter.IsValid()) {
						AttackEmitter.Get()->DeactivateSystem();
					}
					if (attacksLeft <= 0) {
						Disappear();
					}
				}
				, 0.5f, false, 0.5f);

				bAttackAvailable = false;
				attacksLeft--;

				if (attacksLeft > 0) {
					// Cooldown
					GetWorldTimerManager().SetTimer(AttackTimerHandle,
						[this] {
						bAttackAvailable = true;
					}
					, attackCooldown, false, attackCooldown);
				}
			}
		}
	}
}

void ASparkBallAbility::GetDashedByPlayer(AActor* PlayerActor) {
	// If the ball is not charged and the player dashes through it will be charged
	if (!bCharged) {
		Charge();
	}
	// If the ball is charged and the player dashes through it will be thrown in the direction of the dash
	else {
		timeAliveLeft = maxTimeAlive;

		MovementDirection = PlayerActor->GetActorForwardVector();
		SphereCollision->AddImpulse(MovementDirection * SphereCollision->GetMass() * 8000);
	}
}

void ASparkBallAbility::GetHit(float damage)
{
	if (!bCharged) {
		currentLife -= damage;
		UE_LOG(LogTemp, Warning, TEXT("Damaged"));
		if (currentLife <= 0) {
			Disappear();
		}
	}
}

void ASparkBallAbility::Disappear()
{
	if (!bDisappearing) {
		bDisappearing = true;
		// Disable the actor
		//SetActorHiddenInGame(true);
		SetActorEnableCollision(false);
		SetActorTickEnabled(false);

		// Death animation
		Emitter.Get()->DestroyComponent();
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), DeathParticleSystem.Get(), GetActorLocation());
		if (AttackEmitter.IsValid()) {
			AttackEmitter.Get()->DestroyComponent();
		}
		if (AttackRangeEmitter.IsValid()) {
			AttackRangeEmitter.Get()->DestroyComponent();
		}

		SetLifeSpan(2.0f);
	}
}