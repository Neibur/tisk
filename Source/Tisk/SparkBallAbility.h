// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SparkBallAbility.generated.h"

UCLASS()
class TISK_API ASparkBallAbility : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASparkBallAbility();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/*UFUNCTION()
		void ActorOverlapBegin(AActor* Me, AActor* OtherActor);*/

	UFUNCTION()
		void AttackSphereOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	UFUNCTION()
		void InnerSphereOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	/*UFUNCTION()
		void InnerSphereOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);*/

	void GetDashedByPlayer(AActor* PlayerActor);

	void GetHit(float damage);

protected:
	//TWeakObjectPtr<UPrimitiveComponent> DummyRoot;

	float maxLife;
	float currentLife;

	bool bCharged;
	bool bDisappearing;

	FTimerHandle AttackTimerHandle;
	bool bAttackAvailable;
	float attackCooldown;

	int maxAttacks;
	int attacksLeft;

	FTimerHandle TimeAliveTimerHandle;
	float maxTimeAlive;
	float timeAliveLeft;

	float movementSpeed;
	FVector MovementDirection;

	TWeakObjectPtr<class AFireballEnemy> EnemyToAttack;

	TWeakObjectPtr<class USphereComponent> SphereCollision;
	TWeakObjectPtr<class USphereComponent> AttackSphereCollision;

	TWeakObjectPtr<class UParticleSystem> SparkParticleSystem;
	TWeakObjectPtr<class UParticleSystem> ChargedSparkParticleSystem;
	TWeakObjectPtr<class UParticleSystem> AttackRangeParticleSystem;
	TWeakObjectPtr<class UParticleSystem> AttackParticleSystem;
	TWeakObjectPtr<class UParticleSystem> DeathParticleSystem;

	TWeakObjectPtr<class UParticleSystemComponent> Emitter;
	TWeakObjectPtr<class UParticleSystemComponent> AttackRangeEmitter;
	TWeakObjectPtr<class UParticleSystemComponent> AttackEmitter;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void Charge();
	void PerformAttack();
	void Disappear();
};
