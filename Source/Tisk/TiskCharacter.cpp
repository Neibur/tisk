// Fill out your copyright notice in the Description page of Project Settings.


#include "TiskCharacter.h"

#include "EngineMinimal.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/Classes/Components/SkeletalMeshComponent.h"
#include "TiskGameModeBase.h"
#include "TiskGameStateBase.h"
#include "TiskPlayerController.h"
#include "UObject/ConstructorHelpers.h"
#include "Sound/SoundCue.h"


// Sets default values
ATiskCharacter::ATiskCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	// Set size for player capsule -> default 42 x 96
	GetCapsuleComponent()->InitCapsuleSize(42.0f, 42.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 1000.0f;
	CameraBoom->RelativeRotation = FRotator(-60.f, 0.f, 0.f);
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	auto SparkParticleSystemAsset = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/Player/Particles/P_Spark_Player.P_Spark_Player'"));
	if (SparkParticleSystemAsset.Succeeded())
	{
		SparkParticleSystem = SparkParticleSystemAsset.Object;
	}

	auto DeathParticleSystemAsset = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/Player/Particles/P_PlayerDeath.P_PlayerDeath'"));
	if (DeathParticleSystemAsset.Succeeded())
	{
		DeathParticleSystem = DeathParticleSystemAsset.Object;
	}

	auto SparkSoundCueAsset = ConstructorHelpers::FObjectFinder<USoundCue>(TEXT("SoundCue'/Game/Audio/Spark_Cue.Spark_Cue'"));
	if (SparkSoundCueAsset.Succeeded())
	{
		SparkSound = SparkSoundCueAsset.Object;
	}

	maxLife = 3;
	currentLife = maxLife;
	bDisappearing = false;
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
}

// Called when the game starts or when spawned
void ATiskCharacter::BeginPlay()
{
	Super::BeginPlay();

	GetCharacterMovement()->Mass = 10.0f;

	Tags.Add(FName("TiskCharacter"));

	Emitter = UGameplayStatics::SpawnEmitterAttached(SparkParticleSystem.Get(), RootComponent);
	SparkAudioComponent = UGameplayStatics::SpawnSoundAttached(SparkSound.Get(), RootComponent);
}

// Called every frame
void ATiskCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATiskCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ATiskCharacter::GetHit(float damage)
{
	currentLife -= damage;
	if (currentLife == 0) {
		Disappear();
	}
}

void ATiskCharacter::Disappear()
{
	if (!bDisappearing) {
		bDisappearing = true;
		// Disable the player
		SetActorHiddenInGame(true);
		SetActorEnableCollision(false);
		SetActorTickEnabled(false);

		GetController<ATiskPlayerController>()->SetPlayerMovementEnabled(false);

		SparkAudioComponent.Get()->Deactivate();

		// Notify the GameMode
		GetWorld()->GetAuthGameMode<ATiskGameModeBase>()->SetIsPlayerAlive(false);

		// Death animation
		Emitter.Get()->DestroyComponent();
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), DeathParticleSystem.Get(), GetActorLocation());
	}
}