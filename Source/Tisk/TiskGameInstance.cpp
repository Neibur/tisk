// Fill out your copyright notice in the Description page of Project Settings.


#include "TiskGameInstance.h"
#include "Engine/Engine.h"
#include "TimerManager.h"
#include "TiskGameModeBase.h"
//#include "Engine/World.h"
#include "AchievementManager.h"
#include "AchievementSubject.h"
//#include "Kismet/GameplayStatics.h"
//#include "CustomSaveGame.h"
#include "GameFramework/PlayerController.h"


void UTiskGameInstance::Init() {
	Super::Init();

	AchievementManager = NewObject<UAchievementManager>();

	AchievementSubject = NewObject<UAchievementSubject>();
	AchievementSubject->AddObserver(AchievementManager->_getUObject());

	UWorld* World = WorldContext->World();
	if (World->IsPlayInEditor()) {
		AchievementManager->SetWorld(World);
	}

	FWorldDelegates::OnPostWorldInitialization.AddUObject(this, &UTiskGameInstance::OnMapLoaded);

	bGamepadMode = false;
}

void UTiskGameInstance::OnMapLoaded(UWorld* World, const UWorld::InitializationValues IVS) {
	AchievementManager->SetWorld(World);
}

bool UTiskGameInstance::IsGamepadMode()
{
	return bGamepadMode;
}

void UTiskGameInstance::UpdateGamepadMode(bool newValue)
{
	// Change Input Mode
	TWeakObjectPtr<APlayerController> Controller = GetWorld()->GetFirstPlayerController();
	if (Controller.IsValid()) {
		// Gamepad Mode -> true
		if (newValue) {
			if (!bGamepadMode) {
				//UE_LOG(LogTemp, Warning, TEXT("Gamepad"));
				bGamepadMode = true;
				Controller->bShowMouseCursor = false;
				Controller->bEnableClickEvents = false;
				Controller->bEnableMouseOverEvents = false;

				// Update mouse
				float mouseX, mouseY;
				Controller->GetMousePosition(mouseX, mouseY);
				Controller->SetMouseLocation(mouseX, mouseY);

				FTimerHandle Handle;
				GetTimerManager().SetTimer(Handle,
					[this] {
					float mouseX, mouseY;
					GetWorld()->GetFirstPlayerController()->GetMousePosition(mouseX, mouseY);
					OldMousePosition.Set(mouseX, mouseY);
				}, 0.5f, false, 0.5f);

				GetTimerManager().SetTimer(CheckMouseTimerHandle, this, &UTiskGameInstance::CheckMouse, 1.0f, true, 1.0f);

				TWeakObjectPtr<ATiskGameModeBase> GameMode = GetWorld()->GetAuthGameMode<ATiskGameModeBase>();
				if (GameMode.IsValid()) {
					if (!GameMode->IsUIOut()) {
						FInputModeGameOnly InputMode;
						Controller->SetInputMode(InputMode);
					}
				}
				else {
					FInputModeGameOnly InputMode;
					Controller->SetInputMode(InputMode);
				}
			}
		}
		else {
			// Gamepad Mode -> false
			if (bGamepadMode) {
				bGamepadMode = false;

				GetTimerManager().ClearTimer(CheckMouseTimerHandle);

				TWeakObjectPtr<ATiskGameModeBase> GameMode = GetWorld()->GetAuthGameMode<ATiskGameModeBase>();
				if (GameMode.IsValid()) {
					if (GameMode->IsUIOut()) {
						Controller->bShowMouseCursor = true;
						Controller->bEnableClickEvents = true;
						Controller->bEnableMouseOverEvents = true;

						FInputModeGameAndUI InputMode;
						InputMode.SetHideCursorDuringCapture(false);
						Controller->SetInputMode(InputMode);

						// Update mouse
						float mouseX, mouseY;
						Controller->GetMousePosition(mouseX, mouseY);
						Controller->SetMouseLocation(mouseX, mouseY);
					}
				}
				else {
					Controller->bShowMouseCursor = true;
					Controller->bEnableClickEvents = true;
					Controller->bEnableMouseOverEvents = true;

					FInputModeGameAndUI InputMode;
					InputMode.SetHideCursorDuringCapture(false);
					Controller->SetInputMode(InputMode);

					// Update mouse
					float mouseX, mouseY;
					Controller->GetMousePosition(mouseX, mouseY);
					Controller->SetMouseLocation(mouseX, mouseY);
				}

			}
		}
	}
}

void UTiskGameInstance::CheckMouse()
{
	TWeakObjectPtr<APlayerController> Controller = GetWorld()->GetFirstPlayerController();
	float mouseX, mouseY;
	GetWorld()->GetFirstPlayerController()->GetMousePosition(mouseX, mouseY);
	// Check if the mouse has moved
	if (OldMousePosition.X != mouseX
		|| OldMousePosition.Y != mouseY) {
		// Switch to not gamepad mode and stop the timer
		bGamepadMode = false;

		TWeakObjectPtr<ATiskGameModeBase> GameMode = GetWorld()->GetAuthGameMode<ATiskGameModeBase>();
		if (GameMode.IsValid()) {
			if (GameMode->IsUIOut()) {
				Controller->bShowMouseCursor = true;
				Controller->bEnableClickEvents = true;
				Controller->bEnableMouseOverEvents = true;
			}
		}
		else {
			Controller->bShowMouseCursor = true;
			Controller->bEnableClickEvents = true;
			Controller->bEnableMouseOverEvents = true;
		}

		GetTimerManager().ClearTimer(CheckMouseTimerHandle);
	}
}