// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Engine/World.h"
#include "TiskGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TISK_API UTiskGameInstance : public UGameInstance
{
	GENERATED_BODY()
	

public:
	void Init() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Achievements")
		class UAchievementManager* AchievementManager;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Achievements")
		class UAchievementSubject* AchievementSubject;

	UFUNCTION(BlueprintCallable)
		bool IsGamepadMode();

	UFUNCTION(BlueprintCallable)
		void UpdateGamepadMode(bool value);

protected:
	bool bGamepadMode;
	FVector2D OldMousePosition;

	FTimerHandle CheckMouseTimerHandle;

	UFUNCTION()
		void CheckMouse();

private:
	void OnMapLoaded(UWorld* World, const UWorld::InitializationValues IVS);
};
