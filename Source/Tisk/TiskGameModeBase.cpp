// Fill out your copyright notice in the Description page of Project Settings.


#include "TiskGameModeBase.h"

#include "TiskCharacter.h"
#include "TiskPlayerController.h"
#include "UObject/ConstructorHelpers.h"
#include "TiskGameStateBase.h"
#include "LevelHUD.h"
#include "Runtime/Engine/Classes/GameFramework/HUD.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "FireballEnemy.h"
#include "Engine/TargetPoint.h"
#include "Engine/World.h"
#include "AIController.h"
#include "BrainComponent.h"
#include "TiskGameInstance.h"
#include "AchievementSubject.h"

ATiskGameModeBase::ATiskGameModeBase()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATiskPlayerController::StaticClass();

	DefaultPawnClass = ATiskCharacter::StaticClass();

	// GameState
	GameStateClass = ATiskGameStateBase::StaticClass();

	// HUD
	auto CH_MainHUD = ConstructorHelpers::FClassFinder<AHUD>(TEXT("/Game/UI/GS_Widgets/LevelHUD_BP"));
	if (CH_MainHUD.Succeeded()) {
		HUDClass = CH_MainHUD.Class;
	}

	bPlayerAlive = true;
}

void ATiskGameModeBase::KillEnemy() {
	enemyCount--;

	GetGameState<ATiskGameStateBase>()->enemiesLeft = enemyCount;

	// The player has won
	if (enemyCount == 0 && bPlayerAlive) {
		TWeakObjectPtr<ATiskPlayerController> PlayerController = GetWorld()->GetFirstPlayerController<ATiskPlayerController>();
		PlayerController->SetIgnoreMoveInput(true);
		PlayerController->SetPlayerMovementEnabled(false);
		PlayerController->GetPawn()->SetActorEnableCollision(false);

		FInputModeGameAndUI InputMode;
		InputMode.SetHideCursorDuringCapture(true);
		PlayerController->SetInputMode(InputMode);

		UAchievementSubject* aS = GetGameInstance<UTiskGameInstance>()->AchievementSubject;
		if (aS) {
			GetGameInstance<UTiskGameInstance>()->AchievementSubject->Notify(this, EEvent::EVENT_WIN_LEVEL);
		}

		FinishLevel(true);
	}
}

bool ATiskGameModeBase::IsPlayerAlive() {
	return bPlayerAlive;
}

void ATiskGameModeBase::SetIsPlayerAlive(bool alive) {
	bPlayerAlive = alive;

	GetGameState<ATiskGameStateBase>()->bPlayerAlive = alive;

	// The player has lost
	if (!alive) {
		TWeakObjectPtr<ATiskPlayerController> PlayerController = GetWorld()->GetFirstPlayerController<ATiskPlayerController>();

		PlayerController->SetIgnoreMoveInput(true);
		PlayerController->GetPawn()->SetActorTickEnabled(false);

		FInputModeGameAndUI InputMode;
		InputMode.SetHideCursorDuringCapture(true);
		GetWorld()->GetFirstPlayerController()->SetInputMode(InputMode);

		// Stop enemy AI
		TArray<AActor*> Enemies;
		UGameplayStatics::GetAllActorsOfClass(this, AFireballEnemy::StaticClass(), Enemies);

		for (AActor* actor : Enemies) {
			TWeakObjectPtr<AFireballEnemy> fireballEnemy = Cast<AFireballEnemy>(actor);
			Cast<AAIController>(fireballEnemy->GetController())->GetBrainComponent()->StopLogic("PlayerIsDead");
		}

		FinishLevel(false);
	}
}

bool ATiskGameModeBase::IsUIOut()
{
	return bUIOut;
}

TWeakObjectPtr<ALevelHUD> ATiskGameModeBase::GetLevelHUD()
{
	return LevelHUD;
}

void ATiskGameModeBase::ReceiveUINavigation(int NavigationType)
{
	if (bUIOut) {
		LevelHUD->ReceiveNavRequest(NavigationType);
	}
}

void ATiskGameModeBase::TriggerUIButton()
{
	if (bUIOut) {
		LevelHUD->TriggerButton();
	}
}

void ATiskGameModeBase::FinishLevel(bool LevelWon)
{
	bUIOut = true;

	TWeakObjectPtr<ATiskPlayerController> PlayerController = GetWorld()->GetFirstPlayerController<ATiskPlayerController>();
	if (!GetGameInstance<UTiskGameInstance>()->IsGamepadMode()) {
		PlayerController->bShowMouseCursor = true;
		PlayerController->bEnableClickEvents = true;
		PlayerController->bEnableMouseOverEvents = true;

		FInputModeGameAndUI InputMode;
		InputMode.SetHideCursorDuringCapture(false);
		PlayerController->SetInputMode(InputMode);

		// Update mouse
		float mouseX, mouseY;
		PlayerController->GetMousePosition(mouseX, mouseY);
		PlayerController->SetMouseLocation(mouseX, mouseY);
	}

	LevelHUD->ShowLevelEndWidget(LevelWon);
}

FString ATiskGameModeBase::InitNewPlayer(APlayerController* NewPlayerController, const FUniqueNetIdRepl& UniqueId, const FString& Options, const FString& Portal)
{
	bPlayerAlive = true;
	GetGameState<ATiskGameStateBase>()->bPlayerAlive = true;

	return Super::InitNewPlayer(NewPlayerController, UniqueId, Options, Portal);
}

void ATiskGameModeBase::StartPlay()
{
	Super::StartPlay();

	bUIOut = false;

	// Get the HUD
	LevelHUD = Cast<ALevelHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());

	// Spawn Enemies
	TArray<AActor*> EnemySpawns;
	UGameplayStatics::GetAllActorsWithTag(this, FName("EnemySpawn"), EnemySpawns);

	if (EnemySpawns.Num() > 0) {
		enemyCount = EnemySpawns.Num();
		for (AActor* spawn : EnemySpawns) {
			GetWorld()->SpawnActor<AFireballEnemy>(spawn->GetActorLocation(), FRotator(0, 0, 0));
		}
	}

	// Set Input Mode to Game
	LevelHUD->SetInputModeToGame();
}

void ATiskGameModeBase::StartToLeaveMap()
{
	Super::StartToLeaveMap();
}
