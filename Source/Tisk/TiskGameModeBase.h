// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TiskGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TISK_API ATiskGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	ATiskGameModeBase();

	void KillEnemy();

	bool IsPlayerAlive();
	void SetIsPlayerAlive(bool alive);

	bool IsUIOut();
	TWeakObjectPtr<class ALevelHUD> GetLevelHUD();
	void ReceiveUINavigation(int NavigationType);
	void TriggerUIButton();

	virtual FString InitNewPlayer(APlayerController* NewPlayerController, const FUniqueNetIdRepl& UniqueId, const FString& Options, const FString& Portal);
	virtual void StartPlay();
	virtual void StartToLeaveMap();

private:
	TWeakObjectPtr<class ALevelHUD> LevelHUD;

	bool bPlayerAlive;
	int enemyCount;

	bool bUIOut;

	void FinishLevel(bool LevelWon);
};
