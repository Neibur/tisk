// Fill out your copyright notice in the Description page of Project Settings.


#include "TiskGameStateBase.h"
#include "TiskGameModeBase.h"
#include "Engine/World.h"


ATiskGameStateBase::ATiskGameStateBase()
{
	bPlayerAlive = true;

	enemiesLeft = 0;
}

void ATiskGameStateBase::RestartCurrentLevel()
{
	GetWorld()->GetAuthGameMode<ATiskGameModeBase>()->ResetLevel();
}