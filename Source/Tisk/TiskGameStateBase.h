// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "TiskGameStateBase.generated.h"

/**
 * 
 */
UCLASS()
class TISK_API ATiskGameStateBase : public AGameStateBase
{
	GENERATED_BODY()
	

public:
	ATiskGameStateBase();

	UFUNCTION(BlueprintCallable)
		void RestartCurrentLevel();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bPlayerAlive;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int enemiesLeft;

};
