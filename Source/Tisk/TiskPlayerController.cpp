// Fill out your copyright notice in the Description page of Project Settings.


#include "TiskPlayerController.h"
#include "EngineMinimal.h"
#include "UObject/ConstructorHelpers.h"
#include "TiskCharacter.h"
#include "ParryShield.h"
#include "SparkBallAbility.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Engine/World.h"
#include "TiskGameModeBase.h"
#include "TiskGameInstance.h"

ATiskPlayerController::ATiskPlayerController()
{
	//bShowMouseCursor = true;
	//DefaultMouseCursor = EMouseCursor::Crosshairs;

	auto SparkBallCooldownParticleSystemAsset = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/Player/Particles/P_SparkBall_Cooldown.P_SparkBall_Cooldown'"));
	if (SparkBallCooldownParticleSystemAsset.Succeeded())
	{
		SparkBallCooldownParticleSystem = SparkBallCooldownParticleSystemAsset.Object;
	}

	bPlayerMovementEnabled = true;

	// Dash
	bChargingDash = false;
	largeDashCountdown = 1.0f;

	bDashAvailable = true;
	dashCooldownTime = 0.5f;

	// Parry
	bParryAvailable = true;
	parryCooldownTime = 1.0f;

	// Sparkball
	bSparkballAvailable = true;
	sparkballCooldownTime = 4.0f;
}

void ATiskPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	// Keeping track of the time the dash is being charged
	if (bPlayerMovementEnabled && bChargingDash && (largeDashCountdown > 0.2f))
	{
		largeDashCountdown -= 0.02f;
	}
}

void ATiskPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();
	
	// ---- Input Device Change ----
	InputComponent->BindAction("CheckInputDevice", IE_Pressed, this, &ATiskPlayerController::InputReceived);

	// ---- UI Controls ----
	InputComponent->BindAction("UIGoUp", IE_Pressed, this, &ATiskPlayerController::UIGoUp);
	InputComponent->BindAction("UIGoDown", IE_Pressed, this, &ATiskPlayerController::UIGoDown);
	InputComponent->BindAction("UITriggerButton", IE_Pressed, this, &ATiskPlayerController::UITriggerButton);

	// ---- Dash Ability ----
	InputComponent->BindAction("Dash", IE_Pressed, this, &ATiskPlayerController::ChargeDash);
	InputComponent->BindAction("Dash", IE_Released, this, &ATiskPlayerController::PerformDash);

	// ---- Parry Ability ----
	InputComponent->BindAction("Parry", IE_Pressed, this, &ATiskPlayerController::PerformParry);

	// ---- Spark Ball Ability ----
	InputComponent->BindAction("SparkBall", IE_Pressed, this, &ATiskPlayerController::PerformSparkBall);

	// ---- Movement ----
	InputComponent->BindAxis("MoveForward", this, &ATiskPlayerController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ATiskPlayerController::MoveRight);
}

void ATiskPlayerController::InputReceived(FKey key)
{
	if (key.IsGamepadKey()) {
		GetGameInstance<UTiskGameInstance>()->UpdateGamepadMode(true);
	}
	else {
		GetGameInstance<UTiskGameInstance>()->UpdateGamepadMode(false);
	}
}

void ATiskPlayerController::UIGoUp()
{
	GetWorld()->GetAuthGameMode<ATiskGameModeBase>()->ReceiveUINavigation(0);
}

void ATiskPlayerController::UIGoDown()
{
	GetWorld()->GetAuthGameMode<ATiskGameModeBase>()->ReceiveUINavigation(1);
}

void ATiskPlayerController::UITriggerButton()
{
	GetWorld()->GetAuthGameMode<ATiskGameModeBase>()->TriggerUIButton();
}

void ATiskPlayerController::ChargeDash()
{
	//UE_LOG(LogTemp, Warning, TEXT("ChargeDash"));

	if (bPlayerMovementEnabled && bDashAvailable) {
		bChargingDash = true;
	}
}

void ATiskPlayerController::PerformDash()
{
	//UE_LOG(LogTemp, Warning, TEXT("PerformDash"));
	if (bPlayerMovementEnabled && bDashAvailable && bChargingDash) {
		//bDashAvailable = false;
		bChargingDash = false;

		// Cooldown
		/*GetWorldTimerManager().SetTimer(DashTimerHandle,
			[this] {
			bDashAvailable = true;
		}
		, dashCooldownTime, false);*/

		if (largeDashCountdown <= 0.2f) {
			//UE_LOG(LogTemp, Warning, TEXT("Large Dash"));
			Cast<ACharacter>(GetPawn())->GetCharacterMovement()->AddImpulse((GetPawn()->GetActorForwardVector()) * 140000);
		}
		else {
			//UE_LOG(LogTemp, Warning, TEXT("Basic Dash"));
			Cast<ACharacter>(GetPawn())->GetCharacterMovement()->AddImpulse((GetPawn()->GetActorForwardVector()) * 70000);
		}

		largeDashCountdown = 1.0f;

		// Notify the near SparkBallAbilities
		TArray<AActor*> OverlappingSparkBalls;
		GetPawn()->GetOverlappingActors(OverlappingSparkBalls, ASparkBallAbility::StaticClass());

		if (OverlappingSparkBalls.Num() > 0) {
			for (AActor* actor : OverlappingSparkBalls) {
				if (GetPawn()->GetDistanceTo(actor) < 200) {
					Cast<ASparkBallAbility>(actor)->GetDashedByPlayer(GetPawn());
				}
			}
		}
	}
}

void ATiskPlayerController::PerformParry()
{
	//UE_LOG(LogTemp, Warning, TEXT("Parry"));

	if (bPlayerMovementEnabled && bParryAvailable) {
		bParryAvailable = false;

		// Cooldown
		GetWorldTimerManager().SetTimer(ParryTimerHandle,
			[this] {
			bParryAvailable = true;
		}
		, parryCooldownTime, false);

		AParryShield* parryShield = GetWorld()->SpawnActor<AParryShield>(FVector(0, 0, 0), FRotator(0, 0, 0));
		FAttachmentTransformRules AttachRules = { EAttachmentRule::SnapToTarget, false };
		parryShield->AttachToActor(GetPawn(), AttachRules);
		parryShield->SetActorLocation(GetPawn()->GetActorLocation());
	}
}

void ATiskPlayerController::PerformSparkBall()
{
	//UE_LOG(LogTemp, Warning, TEXT("Spark Ball"));

	if (bPlayerMovementEnabled && bSparkballAvailable) {
		bSparkballAvailable = false;

		ASparkBallAbility* sparkBall = GetWorld()->SpawnActor<ASparkBallAbility>(GetPawn()->GetActorLocation() + FVector(0, 0, 50)
			, FRotator(0, 0, 0));

		// Show the cooldown Particle
		SparkBallCooldownEmitter = UGameplayStatics::SpawnEmitterAttached(SparkBallCooldownParticleSystem.Get(), GetPawn()->GetRootComponent());
		SparkBallCooldownEmitter->SetAbsolute(false, true);

		// Cooldown
		GetWorldTimerManager().SetTimer(SparkballTimerHandle,
			[this] {
			bSparkballAvailable = true;
		}
		, sparkballCooldownTime, false);
	}
}

void ATiskPlayerController::MoveForward(float Value)
{
	if (bPlayerMovementEnabled && (Value != 0.0f) && (GetPawn() != nullptr)) {
		// find out which way is forward
		const FRotator Rotation = GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		
		// If the player is charging the dash slow down the movement speed
		if (bChargingDash && (largeDashCountdown <= 0.7f)) {
			GetPawn()->AddMovementInput(Direction, Value * largeDashCountdown);
		}
		else {
			GetPawn()->AddMovementInput(Direction, Value);
		}
	}
}

void ATiskPlayerController::MoveRight(float Value)
{
	if (bPlayerMovementEnabled && (Value != 0.0f) && (GetPawn() != nullptr)) {
		// find out which way is right
		const FRotator Rotation = GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		// If the player is charging the dash slow down the movement speed
		if (bChargingDash && (largeDashCountdown <= 0.7f)) {
			GetPawn()->AddMovementInput(Direction, Value * largeDashCountdown);
		}
		else {
			GetPawn()->AddMovementInput(Direction, Value);
		}
	}
}

bool ATiskPlayerController::GetPlayerMovementEnabled() {
	return bPlayerMovementEnabled;
}

void ATiskPlayerController::SetPlayerMovementEnabled(bool enabled) {
	bPlayerMovementEnabled = enabled;
}

void ATiskPlayerController::OnPossess(APawn* InPawn) {
	Super::OnPossess(InPawn);
}

void ATiskPlayerController::OnUnPossess() {
	Super::OnUnPossess();
}