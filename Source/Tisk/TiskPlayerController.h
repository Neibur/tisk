// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TiskPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class TISK_API ATiskPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	ATiskPlayerController();

	bool GetPlayerMovementEnabled();
	void SetPlayerMovementEnabled(bool enabled);
protected:
	bool bPlayerMovementEnabled;

	bool bChargingDash;
	float largeDashCountdown;

	FTimerHandle DashTimerHandle;
	bool bDashAvailable;
	float dashCooldownTime;

	FTimerHandle ParryTimerHandle;
	bool bParryAvailable;
	float parryCooldownTime;

	FTimerHandle SparkballTimerHandle;
	bool bSparkballAvailable;
	float sparkballCooldownTime;
	TWeakObjectPtr<class UParticleSystem> SparkBallCooldownParticleSystem;
	TWeakObjectPtr<class UParticleSystemComponent> SparkBallCooldownEmitter;

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;

	/** Called when any input is received */
	void InputReceived(FKey key);

	/** Called to move through the UI */
	void UIGoUp();
	void UIGoDown();
	void UITriggerButton();

	/** Called to charge the Dash movement */
	void ChargeDash();

	/** Called to perform the Dash ability */
	void PerformDash();

	/** Called to perform the Parry ability */
	void PerformParry();

	/** Called to perform the Spark Ball ability */
	void PerformSparkBall();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

protected:
	virtual void OnPossess(APawn* InPawn) override;
	virtual void OnUnPossess() override;
};
